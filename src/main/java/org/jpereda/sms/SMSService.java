package org.jpereda.sms;

import java.util.List;
import javafx.beans.property.ReadOnlyObjectProperty;

/**
 *
 * @author jpereda
 */
public interface SMSService {
    
    ReadOnlyObjectProperty<SMSMessage> messagesProperty();
    
    void sendSMS(String number, String message);

    List<SMSMessage> readSMSs();

    void listenToIncomingSMS();

}
