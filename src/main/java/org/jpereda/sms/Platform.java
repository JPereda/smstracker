package org.jpereda.sms;

/**
 *
 * @author jpereda
 */
public abstract class Platform {
    
    public abstract SMSService getSMSService();
    
}
