package org.jpereda.sms;

/**
 *
 * @author jpereda
 */
public class AndroidPlatform extends Platform {

    private AndroidSMSService androidSMSService;

    @Override
    public SMSService getSMSService() {
        if(androidSMSService==null){
            androidSMSService=new AndroidSMSService();
        }
        return androidSMSService;
    }
    
}
