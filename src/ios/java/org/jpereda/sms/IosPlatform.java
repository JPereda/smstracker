package org.jpereda.sms;

/**
 *
 * @author jpereda
 */
public class IosPlatform extends Platform {

    private IosSMSService iosSMSService;

    @Override
    public SMSService getSMSService() {
        if(iosSMSService==null){
            iosSMSService=new IosSMSService();
        }
        return iosSMSService;
    }

}