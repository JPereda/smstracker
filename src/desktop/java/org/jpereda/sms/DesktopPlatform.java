package org.jpereda.sms;

/**
 *
 * @author jpereda
 */
public class DesktopPlatform extends Platform {

    private DesktopSMSService desktopSMSService;

    @Override
    public SMSService getSMSService() {
        if(desktopSMSService==null){
            desktopSMSService=new DesktopSMSService();
        }
        return desktopSMSService;
    }

}
